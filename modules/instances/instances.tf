resource "google_compute_instance" "instances" {
  name         = "tf-instance-1"
  machine_type = var.machine_type

  boot_disk {
    initialize_params {
      image = "debian-10-buster-v20210817"
    }
  }

  network_interface {
    network    = "terraform-vpc"
    subnetwork = "subnet-01"
    access_config {
    }
  }

  metadata_startup_script   = <<-EOT
        #!/bin/bash
    EOT
  allow_stopping_for_update = true

}

resource "google_compute_instance" "instances2" {
  name         = "tf-instance-2"
  machine_type = var.machine_type

  boot_disk {
    initialize_params {
      image = "debian-10-buster-v20210817"
    }
  }

  network_interface {
    network    = "terraform-vpc"
    subnetwork = "subnet-02"
    access_config {
    }
  }

  metadata_startup_script   = <<-EOT
        #!/bin/bash
    EOT
  allow_stopping_for_update = true

}

resource "google_compute_instance" "instances3" {
  name         = "tf-instance-3"
  machine_type = var.machine_type

  boot_disk {
    initialize_params {
      image = "debian-10-buster-v20210817"
    }
  }

  network_interface {
    network    = "terraform-vpc"
    subnetwork = "subnet-02"
    access_config {
    }
  }

  metadata_startup_script   = <<-EOT
        #!/bin/bash
    EOT
  allow_stopping_for_update = true

}

