provider "google" {

  project = var.project_id
  region  = var.region
  zone    = var.zone
  credentials = terraform.workspace == "prod"? var.KEYPROD : var.KEYDEV
}

module "instances" {
  source = "./modules/instances"
  depends_on = [
    module.vpc
  ]
}

module "storage" {
  source = "./modules/storage"
}

terraform {
  backend "local" {
    path = "terraform/state/terraform.tfstate"
  }
}


module "vpc" {
  source  = "terraform-google-modules/network/google"
  version = "3.2.2"

  project_id   = var.project_id
  network_name = "terraform-vpc"
  routing_mode = "GLOBAL"

  subnets = [
    {
      subnet_name   = "subnet-01"
      subnet_ip     = "10.10.10.0/24"
      subnet_region = "us-central1"
    },
    {
      subnet_name   = "subnet-02"
      subnet_ip     = "10.10.20.0/24"
      subnet_region = "us-central1"
    }
  ]

}

resource "google_compute_firewall" "default" {
  name    = "test-firewall"
  network = module.vpc.network_self_link

  allow {
    protocol = "tcp"
    ports    = ["80"]
  }

  source_ranges = ["0.0.0.0/0"]
}