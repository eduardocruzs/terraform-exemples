variable "region" {
  description = "A região padrão"
  type        = string
  default     = "us-central1"
}

variable "zone" {
  description = "A zona padrão"
  type        = string
  default     = "us-central1-a"
}

variable "project_id" {
  description = "The ID of the project to create the bucket in."
  type        = string
  default     = "terraformteste0610"
}
